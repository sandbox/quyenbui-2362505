<?php

namespace Drupal\bean_import\Controllers;

class ImportProcess
{
  private $dirExtractTo;
  private $formState;
  private $entity;
  private $fileCreated = array();
  private $override = false;
  private $beanFields = null;
  private $beanFieldsNotFound = array();

  public function __construct($formState, $override = false)
  {
    $this->formState = $formState;
    $this->dirExtractTo = 'temporary://bean_import-' . time();
    $this->override = $override;
    $this->init();
  }

  public function process()
  {
    // 1, Unzip file
    if ($this->unzipFile()) {
      // 2, Read entity data
      $this->entity = $this->readEntityData();
      // 3, Build bean object and save
      $this->beanObjectParseAndSave();
    }
  }

  private function init()
  {
    drupal_mkdir($this->dirExtractTo);
  }

  private function readEntityData()
  {
    $entityFile = drupal_realpath($this->dirExtractTo) . '/entity.serialize.bin';
    if (is_file($entityFile)) {
      return unserialize(file_get_contents($entityFile));
    }

    return NULL;
  }

  private function unzipFile()
  {
    $zipArchive = new \ZipArchive();
    $result = $zipArchive->open($_FILES['files']['tmp_name']['file_import']);
    if ($result === TRUE) {
      $zipArchive->extractTo(drupal_realpath($this->dirExtractTo));
      $zipArchive->close();

      return TRUE;
    } else {
      throw new \Exception('Cant unzip your zip file.');
    }

    return FALSE;
  }

  private function fileCreateNew()
  {
    foreach ($this->entity as &$entityObject) {
      foreach ($entityObject['fields'] as $fieldName => &$field) {
        if (isset($field['is_file']) && $field['is_file']) {
          $fieldSetting = field_info_instance('bean', $fieldName, $entityObject['properties']['type']);
          foreach ($field['data'] as &$fv) {
            foreach ($fv as &$v) {
              if (isset($v['file_info']['fid']) && isset($v['file'])) {
                $file_exists = file_load($v['file_info']['fid']);
                // Create new file
                $fileCheck = $this->override === TRUE && bean_load($entityObject['properties']['delta']);
                if ($fileCheck === FALSE || !$file_exists) {
                  $this->_fileCreateNew($fieldSetting, $v);
                }
                // Use existing file
                if ($file_exists) {
                  $v = (array) $file_exists;
                  $v = isset($v->file_display) ? $v->file_display : 1;
                }
              }
            }
          }
        }
      }
    }
  }

  private function _fileCreateNew($fieldSetting, &$v)
  {
    global $user;
    unset($v['file_info']['fid']);
    $directory = $fieldSetting['settings']['file_directory'] ? $fieldSetting['settings']['file_directory'] : null;
    $fileUri = file_unmanaged_move(drupal_realpath($this->dirExtractTo . '/' . $v['file']), $directory, FILE_EXISTS_RENAME);
    // FIle object
    $file = (object) $v['file_info'];
    $file->uri = $fileUri;
    $file->uid = $user->uid;
    $v = (array) file_save($file);
    $this->fileCreated[] = $v;
    unset($v['file']);
  }

  private function beanTypeFieldCheck($fields)
  {
    if ($this->beanFields === NULL) {
      $bean_fields = field_info_instances('bean');
      foreach (array_shift($bean_fields) as $field) {
        $this->beanFields[$field['field_name']] = $field['field_name'];
      }
    }
    foreach (array_keys($fields) as $field) {
      if (!in_array($field, $this->beanFields)) {
        $this->beanFieldsNotFound[$field] = $field;
      }
    }
  }

  private function beanObjectParseAndSave()
  {
    $imported = 0;
    $this->fileCreateNew();
    foreach ($this->entity as $entity) {
      foreach ($entity['fields'] as &$field) {
        $field = $field['data'];
      }
      // If bean save error delete the files
      $this->beanTypeFieldCheck($entity['fields']);
      $bean = bean_create(array_merge($entity['fields'], $entity['properties']));
      if (!bean_type_load($bean->type)) {
        drupal_set_message(t('Bean type @type do not exists.', array('@type' => $bean->type)));

        return FALSE;
      }
      $exstingBean = bean_load($bean->delta);
      if (!$exstingBean || $this->override) {
        // Update bean
        if ($this->override && $exstingBean) {
          foreach (array('bid', 'vid', 'created', 'changed', 'default_revision', 'uid', 'revisions', 'log') as $property) {
            $bean->{$property} = $exstingBean->{$property};
          }
          unset($bean->is_new);
        }
        // Save bean
        if (!bean_save($bean)) {
          foreach ($this->fileCreated as $file) {
            file_delete($file);
          }

          return FALSE;
        }
        $imported++;
      }
    }
    if ($this->beanFieldsNotFound) {
      drupal_set_message(t('Some fields not found in this site [@field]', array('@field' => implode(', ', $this->beanFieldsNotFound))), 'warning');
    }
    if ($imported > 0) {
      drupal_set_message(t('Bean saved ' . '(' . $imported . ')'));
    } else {
      drupal_set_message(t('No bean have been import.'));
    }

    return TRUE;
  }

}
