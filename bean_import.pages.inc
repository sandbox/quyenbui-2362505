<?php

function bean_import_form() {
  $form = array();
  $form['file_import'] = array(
    '#type' => 'file',
    '#title' => t('Choose a file to import'),
    '#description' => t('This is a zip compress file, You can make it by use bean_export module'),
    '#reqired' => TRUE
  );
  $form['override_existing_bean'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override the beans if it already existing.'),
    '#default_value' => FALSE
  );
  $form['actions']['import'] = array(
    '#type' => 'submit',
    '#value' => 'Import',
    '#submit' => array('bean_import_form_submit_import')
  );
  $form['#validate'] = array('bean_import_form_validate');
  return $form;
}

function bean_import_form_submit_import($form, &$form_state) {
  at_id(new Drupal\bean_import\Controllers\ImportProcess($form_state, (boolean) $form_state['values']['override_existing_bean']))->process();
}

function bean_import_form_validate($form, &$form_state) {
  if ($_FILES['files']['size']['file_import'] < 1) {
    form_set_error('files][file_import', t('Please upload a file to import.'));
  }
  // Required zip format
  if ($_FILES['files']['type']['file_import'] !== 'application/zip') {
    form_set_error('files][file_import', t('You have uploaded invalid file extension.'));
  }
}
